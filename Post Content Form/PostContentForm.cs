﻿namespace API_Response_Viewer
{
    public partial class PostContentForm : Form
    {
        public PostContentForm()
        {
            InitializeComponent();
        }

        private void buttonPostContent_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        public string GetContent()
        {
            return textBoxPostContent.Text;
        }
    }
}
