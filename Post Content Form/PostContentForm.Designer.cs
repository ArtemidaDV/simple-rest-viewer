﻿namespace API_Response_Viewer
{
    partial class PostContentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            labelPostContent = new Label();
            textBoxPostContent = new TextBox();
            buttonPostContent = new Button();
            SuspendLayout();
            // 
            // labelPostContent
            // 
            labelPostContent.AutoSize = true;
            labelPostContent.Location = new Point(18, 18);
            labelPostContent.Name = "labelPostContent";
            labelPostContent.Size = new Size(209, 25);
            labelPostContent.TabIndex = 0;
            labelPostContent.Text = "Content of POST request";
            // 
            // textBoxPostContent
            // 
            textBoxPostContent.Location = new Point(18, 61);
            textBoxPostContent.Multiline = true;
            textBoxPostContent.Name = "textBoxPostContent";
            textBoxPostContent.Size = new Size(440, 303);
            textBoxPostContent.TabIndex = 1;
            // 
            // buttonPostContent
            // 
            buttonPostContent.Location = new Point(18, 384);
            buttonPostContent.Name = "buttonPostContent";
            buttonPostContent.Size = new Size(440, 43);
            buttonPostContent.TabIndex = 2;
            buttonPostContent.Text = "Send POST";
            buttonPostContent.UseVisualStyleBackColor = true;
            buttonPostContent.Click += buttonPostContent_Click;
            // 
            // PostContentForm
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(484, 450);
            Controls.Add(buttonPostContent);
            Controls.Add(textBoxPostContent);
            Controls.Add(labelPostContent);
            Name = "PostContentForm";
            Text = "Enter content of POST request";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label labelPostContent;
        private TextBox textBoxPostContent;
        private Button buttonPostContent;
    }
}