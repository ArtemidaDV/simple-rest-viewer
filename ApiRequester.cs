﻿namespace API_Response_Viewer
{
    internal class ApiRequester
	{
        static HttpClient client = new HttpClient();

        public static async Task<string> GET(string url)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            string output = await response.Content.ReadAsStringAsync();
            return output;
        }

        public static async Task<string> POST(string url, string content)
        {
            HttpResponseMessage response = await client.PostAsync(url, new StringContent(content));
            string output = await response.Content.ReadAsStringAsync();
            return output;
        }
    }
}
