using System.Runtime.InteropServices;
using System.Text.Json.Nodes;

namespace API_Response_Viewer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private async void buttonGET_Click(object sender, EventArgs e)
        {
            string getOutput = await ApiRequester.GET(textBoxURL.Text);
            string formatted = (JsonNode.Parse(getOutput) ?? "NULL"!).ToString();

            textBoxOutput.Text = formatted;
        }

        private async void buttonPOST_Click(object sender, EventArgs e)
        {
            // �������� ���������� �������

            PostContentForm postContentForm = new PostContentForm();
            postContentForm.ShowDialog();
            string content = postContentForm.GetContent();

            postContentForm.Dispose();

            // ���������� ������ � ������� ���������

            string getOutput = await ApiRequester.POST(textBoxURL.Text, content);
            string formatted = (JsonNode.Parse(getOutput) ?? "NULL"!).ToString();

            textBoxOutput.Text = formatted;
        }
    }
}