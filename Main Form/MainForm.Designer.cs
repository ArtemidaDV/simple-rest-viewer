﻿namespace API_Response_Viewer
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            textBoxOutput = new TextBox();
            labelOutput = new Label();
            buttonPOST = new Button();
            buttonGET = new Button();
            labelURL = new Label();
            textBoxURL = new TextBox();
            contextMenuStrip1 = new ContextMenuStrip(components);
            SuspendLayout();
            // 
            // textBoxOutput
            // 
            textBoxOutput.AcceptsReturn = true;
            textBoxOutput.AcceptsTab = true;
            textBoxOutput.HideSelection = false;
            textBoxOutput.Location = new Point(15, 195);
            textBoxOutput.MaximumSize = new Size(468, 1000);
            textBoxOutput.Multiline = true;
            textBoxOutput.Name = "textBoxOutput";
            textBoxOutput.ReadOnly = true;
            textBoxOutput.ScrollBars = ScrollBars.Vertical;
            textBoxOutput.Size = new Size(468, 232);
            textBoxOutput.TabIndex = 5;
            // 
            // labelOutput
            // 
            labelOutput.AutoSize = true;
            labelOutput.Location = new Point(15, 158);
            labelOutput.Name = "labelOutput";
            labelOutput.Size = new Size(145, 25);
            labelOutput.TabIndex = 4;
            labelOutput.Text = "Text of response:";
            // 
            // buttonPOST
            // 
            buttonPOST.Location = new Point(135, 90);
            buttonPOST.Name = "buttonPOST";
            buttonPOST.Size = new Size(112, 34);
            buttonPOST.TabIndex = 3;
            buttonPOST.Text = "POST";
            buttonPOST.UseVisualStyleBackColor = true;
            buttonPOST.Click += buttonPOST_Click;
            // 
            // buttonGET
            // 
            buttonGET.Location = new Point(15, 90);
            buttonGET.Name = "buttonGET";
            buttonGET.Size = new Size(112, 34);
            buttonGET.TabIndex = 2;
            buttonGET.Text = "GET";
            buttonGET.UseVisualStyleBackColor = true;
            buttonGET.Click += buttonGET_Click;
            // 
            // labelURL
            // 
            labelURL.AutoSize = true;
            labelURL.Location = new Point(15, 15);
            labelURL.Name = "labelURL";
            labelURL.Size = new Size(88, 25);
            labelURL.TabIndex = 1;
            labelURL.Text = "Enter URL";
            // 
            // textBoxURL
            // 
            textBoxURL.Location = new Point(15, 52);
            textBoxURL.Name = "textBoxURL";
            textBoxURL.Size = new Size(468, 31);
            textBoxURL.TabIndex = 0;
            textBoxURL.Text = "https://api.ipify.org?format=json";
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.ImageScalingSize = new Size(24, 24);
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new Size(61, 4);
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(10F, 25F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(503, 450);
            Controls.Add(textBoxOutput);
            Controls.Add(labelOutput);
            Controls.Add(buttonPOST);
            Controls.Add(buttonGET);
            Controls.Add(labelURL);
            Controls.Add(textBoxURL);
            Name = "MainForm";
            Text = "API Response Viewer";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private SplitContainer splitContainer1;
        private TextBox textBoxURL;
        private Button buttonPOST;
        private Button buttonGET;
        private Label labelURL;
        private Label labelOutput;
        private TextBox textBoxOutput;
        private ContextMenuStrip contextMenuStrip1;
    }
}